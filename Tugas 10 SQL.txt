1.Buat Database
	CREATE DATABASE myshop;

2. Membuat Table di Dalam Database
	CREATE TABLE users( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null );
	CREATE TABLE categories( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null );
	CREATE TABLE items( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, description varchar(255) NOT null, price int(8) NOT null, stock int(8) NOT null, category_id int(8), FOREIGN KEY(category_id) REFERENCES categories(id) );
	
3. Memasukkan Data pada Table
	INSERT INTO users(name, email, password) VALUES ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");
	INSERT INTO categories(name) VALUES ("gadget"), ("cloth"), ("men"), ("women"), ("branded");
	INSERT INTO items(name, description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", "4000000", "100", "1"), ("Uniklooh", "baju keren dari brand ternama", "500000", "50", "2"), ("IMHO Watch", "jam tangan anak yang jujur banget", "2000000", "10", "1");
	
4. Mengambil Data dari Database
   a. Mengambil data users
	SELECT id, name, email FROM users;

   b. Mengambil data items
	SELECT * FROM items WHERE price > 1000000;
	SELECT * FROM items WHERE name LIKE "%Watch";

   c. Menampilkan data items join dengan kategori
	SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name AS kategori FROM items INNER JOIN categories on items.category_id = category_id;	

5. Mengubah Data dari Database
	UPDATE items set price=2500000 WHERE id=1;